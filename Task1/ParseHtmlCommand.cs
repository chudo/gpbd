﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Task1
{
    public class ParseDivCommand : Command, ICommand
    {

        public ParseDivCommand(NLog.Logger logger, string text, string filename) : base(logger, text, filename)
        {
        }

        public void Execute()
        {
            Console.WriteLine("ParseDivCommand called");
            int totalDiv = 0;
            Regex regex = new Regex(@"<div>");
            Regex regex2 = new Regex(@"</div>");
            MatchCollection matches = regex.Matches(_text);
            MatchCollection matches2 = regex2.Matches(_text);

            totalDiv = matches.Count >= matches2.Count ? matches2.Count : matches.Count;

            _logger.Info($"{_filename}-ParseDivCommand-{totalDiv}");
        }
    }
}
