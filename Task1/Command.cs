﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task1
{
    public class Command
    {

        protected NLog.Logger _logger;
        protected string _text;
        protected string _filename;
             

        public Command(NLog.Logger logger, string text, string filename)
        {
            _logger = logger;
            _text = text;
            _filename = filename;
        }

        public static List<ICommand> GetCommands(NLog.Logger logger, string filepath)
        {
            string txt = File.ReadAllText(filepath);
            string ext = Path.GetExtension(filepath);

            List<ICommand> commands = new List<ICommand>();

            if (ext == ".html")
            {
                commands.Add(new ParseDivCommand(logger, txt, Path.GetFileName(filepath)));
                //commands.Add(new ParseBraceCommand(logger, txt, Path.GetFileName(filepath)));
            }

            if (ext == ".txt")
            {
                commands.Add(new ParsePunctuationCommand(logger, txt, Path.GetFileName(filepath)));
            }

            if (ext == ".css")
            {
                commands.Add(new ParseBraceCommand(logger, txt, Path.GetFileName(filepath)));
            }

            return commands;
        }
    }
}
