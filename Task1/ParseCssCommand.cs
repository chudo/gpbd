﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Task1
{
    public class ParseBraceCommand : Command, ICommand
    {
        public ParseBraceCommand(NLog.Logger logger, string text, string filename): base(logger, text, filename)
        {
        }

        public void Execute()
        {
            Console.WriteLine("ParseCssCommand called");

            bool openEqualsClosed = false;
            Regex regex = new Regex(@"{");
            Regex regex2 = new Regex(@"}");
            MatchCollection matches = regex.Matches(_text);
            MatchCollection matches2 = regex2.Matches(_text);
            openEqualsClosed = matches.Count == matches2.Count;

            _logger.Info($"{_filename}-ParseBraceCommand-{openEqualsClosed}");
        }
    }
}
