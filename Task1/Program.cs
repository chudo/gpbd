﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task1
{
    class Program
    {        

        static void Main(string[] args)
        {
            string watchDir = ConfigurationManager.AppSettings["watchPath"];

            FileSystemWatcher watcher = new FileSystemWatcher();
            watcher.Path = watchDir;
            
            watcher.Created += new FileSystemEventHandler(OnChanged);

            watcher.EnableRaisingEvents = true;

            Console.WriteLine("For exit press 'q'");
            while (Console.Read() != 'q') ;
        }

        private static void OnChanged(object source, FileSystemEventArgs e)
        {
            try
            {
                NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();
                Console.WriteLine("File: " + e.FullPath + " " + e.ChangeType);

                var cmds = Command.GetCommands(logger, e.FullPath);
                foreach(var cmd in cmds)
                {
                    cmd.Execute();
                }
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);

            }
        }
    }
}
