﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Task1
{
    public class ParsePunctuationCommand : Command, ICommand
    {
        public ParsePunctuationCommand(NLog.Logger logger, string text, string filename) : base(logger, text, filename)
        {
        }

        public void Execute()
        {
            Console.WriteLine("ParseTxtCommand called");

            int totalPunctuationMarks = 0;
            Regex regex = new Regex(@"[\.!?,:;]");
            MatchCollection matches = regex.Matches(_text);
            totalPunctuationMarks = matches.Count;

            _logger.Info($"{_filename}-ParsePunctuationCommand-{totalPunctuationMarks}");
        }

    }
}
